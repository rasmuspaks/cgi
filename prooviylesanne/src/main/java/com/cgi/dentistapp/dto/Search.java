package com.cgi.dentistapp.dto;

/**
 * Created by rasmus on 15/04/2017.
 */
public class Search {
    private String searchTerm;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
}
