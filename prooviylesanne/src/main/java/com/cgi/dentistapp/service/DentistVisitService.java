package com.cgi.dentistapp.service;

import java.security.Timestamp;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    public void addVisit(String dentistName, Date visitTime, LocalTime visitClock) {
        DentistVisitEntity visit = new DentistVisitEntity(dentistName, visitTime, visitClock);
        System.out.println(visitTime);
        dentistVisitDao.create(visit);
    }

    public List<DentistVisitEntity> listVisits () {
        return dentistVisitDao.getAllVisits();
    }

    public List<DentistVisitEntity> listSearchedVisits(String term){
        return dentistVisitDao.getSearchedVisits(term);
    }

    public List<DentistVisitEntity> getDetailedVisit(String ID) {
        return dentistVisitDao.getDetailedVisit(ID);
    }

    public int deleteEntry(String ID) {
        return dentistVisitDao.deleteEntry(ID);
    }

    public boolean checkIfTimeAvailable(String dentistName,Date day, LocalTime clock){
        return dentistVisitDao.checkIfTimeAvailable(dentistName,day, clock);
    }
    public int updateEntry(String visitID, String dentistName, String date, String clock){
        return dentistVisitDao.updateEntry(visitID, dentistName, date, clock);
    }

}
