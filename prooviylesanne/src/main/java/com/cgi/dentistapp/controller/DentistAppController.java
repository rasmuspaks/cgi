package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.dto.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/broneeringud")
    public ModelAndView showAllVisits(Model model) {
        ModelAndView response = new ModelAndView("broneeringud");
        response.addObject("visits", dentistVisitService.listVisits());
        return response;

    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO, Model model) {
        return "form";
    }

    @PostMapping("/")
    public ModelAndView postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult) {
        boolean availability = dentistVisitService.checkIfTimeAvailable(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime(),
                dentistVisitDTO.getVisitClock());
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.toString());
            return new ModelAndView("form");
        }
        else if(!availability){
            ModelAndView error = new ModelAndView("form");
            error.addObject("error", "viga");
            return error;
        }
        ModelAndView results = new ModelAndView("results");
        dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitTime(), dentistVisitDTO.getVisitClock());
        return results;
    }

    @PostMapping(value="/search")
    public ModelAndView getSearch(@RequestParam("search") String searchWord) {
        ModelAndView search = new ModelAndView("broneeringud");
        search.addObject("visits", dentistVisitService.listSearchedVisits(searchWord));
        return search;
    }

    @PostMapping(value = "/specific")
    public ModelAndView showDetailedView(@RequestParam("button") String searchTerm){
        ModelAndView visitPage = new ModelAndView("detailvaade");
        visitPage.addObject("details", dentistVisitService.getDetailedVisit(searchTerm));
        return visitPage;
    }

    @PostMapping(value = "/delete")
    public String deleteEntry(@RequestParam("delete") String deleteVisitID) {
        dentistVisitService.deleteEntry(deleteVisitID);
        return "deleting";
    }

    @PostMapping(value="/update")
    public ModelAndView getUpdate(@RequestParam("name") String dentistName, @RequestParam("date") String date, @RequestParam("time") String clock, @RequestParam("change") String visitID) {
        dentistVisitService.updateEntry(visitID, dentistName, date, clock);
        ModelAndView page = new ModelAndView("detailvaade");
        page.addObject("details", dentistVisitService.getDetailedVisit(visitID));
        return page;

    }






}
