package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.crypto.Data;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.security.Timestamp;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e ORDER BY visit_time ASC , visit_clock DESC ").getResultList();
    }

    public List<DentistVisitEntity> getSearchedVisits(String termin){
        return entityManager.createQuery("SELECT e FROM  DentistVisitEntity e WHERE lower(dentist_name) LIKE '%" +
                termin.toLowerCase() + "%'" + "ORDER BY visit_time ASC , visit_clock DESC ").getResultList();
    }

    public List<DentistVisitEntity> getDetailedVisit(String ID){
        return entityManager.createQuery("SELECT e FROM  DentistVisitEntity e WHERE id = " + Integer.parseInt(ID)).getResultList();
    }

    public int deleteEntry(String ID){
        return entityManager.createQuery("DELETE FROM DentistVisitEntity where id = " + Integer.parseInt(ID)).executeUpdate();

    }

    public boolean checkIfTimeAvailable(String dentistName, Date visitDay, LocalTime clock){
        /*
        try {
            Query query1 = entityManager.createQuery("SELECT e from DentistVisitEntity e where visit_clock between :timeToVisit AND DATEADD('hh', 1, visit_clock) " +
                    "AND lower(dentist_name) = :name AND visit_time = :day");
            query1.setParameter("timeToVisit", clock);
            query1.setParameter("name", dentistName.toLowerCase());
            query1.setParameter("day", visitDay);
            return query1.getResultList().size() == 0;
        } catch (DataIntegrityViolationException e){
            System.out.println(e.getMessage());
        }
        */


         Query query1 = entityManager.createQuery("SELECT e from DentistVisitEntity e where visit_clock=:timeToVisit " +
                "AND lower(dentist_name) = :name AND visit_time = :day");
        query1.setParameter("timeToVisit", clock);
        query1.setParameter("name", dentistName.toLowerCase());
        query1.setParameter("day", visitDay);

        return query1.getResultList().size() == 0;
    }

    public int updateEntry(String visitID, String dentistName, String date, String clock) {
        return entityManager.createQuery("UPDATE DentistVisitEntity  SET dentist_name = " + "'" + dentistName + "'"
                + "WHERE id = " + Integer.parseInt(visitID)
        ).executeUpdate();

    }

}
