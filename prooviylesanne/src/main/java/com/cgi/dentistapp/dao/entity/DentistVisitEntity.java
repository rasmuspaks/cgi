package com.cgi.dentistapp.dao.entity;

import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dentist_visit")
public class DentistVisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "visit_time")
    private Date visitTime;

    @Column(name = "visit_clock")
    private LocalTime visitClock;

    @Column(name = "dentist_name")
    private String dentistName;

    public DentistVisitEntity() {
    }

    public DentistVisitEntity(String dentistName, Date visitTime, LocalTime visitClock) {
        this.setVisitTime(visitTime);
        this.setVisitClock(visitClock);
        this.setDentistName(dentistName);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public LocalTime getVisitClock() {
        return visitClock;
    }

    public void setVisitClock(LocalTime visitClock) {
        this.visitClock = visitClock;
    }

    public void setDentistName(String name){
        this.dentistName = name;
    }

    public String getDentistName(){
        return dentistName;
    }


}
